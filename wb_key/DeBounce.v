//-----------------------------------------------------------------------------
//Debounce.v
// Modulo Anti-Rebote para teclado matricial de 3x4
//Autor: Julieth Acero Velandia
//-----------------------------------------------------------------------------
module DeBounce(input clk, input [2:0] columnas, output reg [2:0] colNoBounce);
reg  [2:0] first ; //Recibe la primera entrada 
reg  [2:0] second ;//Recibe la segunda entrada
reg  [9:0] cnt=10'b0;
reg  [8:0] cnt2=5'd0;
parameter delay= 10'd801;
parameter delay2=delay/4;

always @(posedge clk)

begin
	//==3'b001)||(columnas==3'b010)||(columnas==3'b100))
	if(columnas)begin//1
	if(cnt==9'd0)begin
	first=columnas;
	end
		if(cnt==(delay-1))begin//2
			second=columnas;
			if(first==second)begin//3
			colNoBounce<=first;
			end else begin//3,4
			colNoBounce<=3'b000;
			end	//4
			cnt<=0;
		end else begin//3
			cnt<=cnt+2'd2;
	colNoBounce<=3'b000;
			end
end	

end
endmodule