Integrantes: Erika Julieth Acero Velandia
	    

**Electrónica Digital II -** Universidad Nacional de Colombia Sede Bogotá


El proyecto surge de la necesidad principalmente de disponer de un inventario y un control más eficaz de quien posee un equipo determinado de los que pertenecen a los laboratorios del Departamento de Ingeniería Eléctrica y Electrónica**** inicialmente, posteriormente no se descarta la idea de adaptarlo a los otros laboratorios u otros sistemas dentro de la universidad que ameriten un sistema similar.